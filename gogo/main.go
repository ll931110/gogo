package main

import "fmt"

/*
 * A first few "must have":
 * 1. Board representation
 * 2. Checking legal moves
 * 3. Updating board
 * After that, we will work on the AI brain for Go, and GUI (if possible). We don't like GUI too much, though
 */

const (
	BLACK = 1
	WHITE = -1
	EMPTY = 0
)

/**
 * Board representation of a game. It includes the size of the board, and a 1D array corresponding to the cells of the board
 * We implement simple KO rule: prohibit 1 and 2 move repetition
 */
type Board struct {
	BoardSize int // the size of the board
	BoardValue [][]int
	PreviousZero [][]int
	PreviousOne [][]int
	ReassignPosition int
}

/**
 * Print the board
 */
func (BoardInstance Board) printBoard() {	
	for i := 0; i < BoardInstance.BoardSize; i++ {
		for j := 0; j < BoardInstance.BoardSize; j++ {
			switch (BoardInstance.BoardValue[i][j]) {
				case WHITE:
					fmt.Print("X")
				case BLACK:
					fmt.Print("O")
				case EMPTY:
					fmt.Print(".")
			}
		}
		fmt.Println()
	}
}

/**
 * Check if the position is inside the board
 */
func (BoardInstance Board) isInsideBoard(x, y int) bool {
	return x >= 0 && x < BoardInstance.BoardSize && y >= 0 && y < BoardInstance.BoardSize
}

/**
 * Return the color of (x, y)
 */
func (BoardInstance Board) getColor(x, y int) int {
	// What should we return on (x, y)?
	if !BoardInstance.isInsideBoard(x, y) {
		return EMPTY
	}
	return BoardInstance.BoardValue[x][y]
}

/**
 * Given the color and the turn, remove all surrounded stones of that color
 * Return false if there's no stone removed, and true otherwise
 */
func (BoardInstance Board) checkSurrounding(color int) bool {
	return false // TODO
}

/**
 * Place a stone in position (x, y), whose turn is either BLACK or WHITE
 * Return true if this is a valid move, false otherwise
 */
func (BoardInstance Board) placeStone(x, y, turn int) bool {
	// Check if the stone is inside the board
	if !BoardInstance.isInsideBoard(x, y) {
		fmt.Println("Illegal move: you must place the stone inside the board")
		return false
	}
	
	// can only place stone on non empty cells
	if BoardInstance.getColor(x, y) != EMPTY {
		fmt.Println("Illegal move: you must place the stone on an empty cell")
		return false
	}
	
	// Since Go has aliasing, one needs to create a new board and copy element-wise
	tmpBoard := make([][]int, BoardInstance.BoardSize, BoardInstance.BoardSize)
	for i := 0; i < len(tmpBoard); i++ {
		tmpBoard[i] = make([]int, BoardInstance.BoardSize)
	}

	for i := 0; i < len(tmpBoard); i++ {
		for j := 0; j < len(tmpBoard); j++ {
			tmpBoard[i][j] = BoardInstance.BoardValue[i][j]
		}
	}
	// Now place the stone there and proceed
	tmpBoard[x][y] = turn

	isMarked := make([][]bool, BoardInstance.BoardSize, BoardInstance.BoardSize)
	for i := 0; i < BoardInstance.BoardSize; i++ {
		row := make([]bool, BoardInstance.BoardSize)
		isMarked[i] = row
	}
	// TODO: refactor the BFS part
	queue := make([]int, 0)	
	for i := 0; i < BoardInstance.BoardSize; i++ {
		for j := 0; j < BoardInstance.BoardSize; j++ {
			if tmpBoard[i][j] == EMPTY {
				queue = append(queue, i)
				queue = append(queue, j)
				isMarked[i][j] = true
			} else {
				isMarked[i][j] = false
			}
		}
	}

	dx := [...]int {-1, 0, 1, 0}
	dy := [...]int {0, 1, 0, -1}

	// A cell is not surrounded, if there is an orthogonal path from that cell to an empty cell
	for ; len(queue) > 0 ; {
		firstX := queue[0]
		firstY := queue[1]
		queue = queue[2:]

		for d := 0; d < len(dx); d++ {
			lastX := firstX + dx[d]
			lastY := firstY + dy[d]
			if BoardInstance.isInsideBoard(lastX, lastY) && !isMarked[lastX][lastY] && tmpBoard[lastX][lastY] != turn {
					isMarked[lastX][lastY] = true
					queue = append(queue, lastX)
					queue = append(queue, lastY)
				}
		}
	}

	// Remove opponent cells that are surrounded
	for i := 0; i < len(tmpBoard); i++ {
		for j := 0; j < len(tmpBoard); j++ {
			if tmpBoard[i][j] != turn && !isMarked[i][j] {
				tmpBoard[i][j] = EMPTY
			}
		}
	}

	// Now BFS on the our own cells
	for i := 0; i < BoardInstance.BoardSize; i++ {
		for j := 0; j < BoardInstance.BoardSize; j++ {
			if tmpBoard[i][j] == EMPTY {
				queue = append(queue, i)
				queue = append(queue, j)
				isMarked[i][j] = true
			}
		}
	}

	for ; len(queue) > 0 ; {
		firstX := queue[0]
		firstY := queue[1]
		queue = queue[2:]

		for d := 0; d < len(dx); d++ {
			lastX := firstX + dx[d]
			lastY := firstY + dy[d]
			if BoardInstance.isInsideBoard(lastX, lastY) && !isMarked[lastX][lastY] && tmpBoard[lastX][lastY] == turn {
					isMarked[lastX][lastY] = true
					queue = append(queue, lastX)
					queue = append(queue, lastY)
				}
		}
	}

	isSuicideMove := false
	for i := 0; i < len(tmpBoard); i++ {
		for j := 0; j < len(tmpBoard); j++ {
			if tmpBoard[i][j] == turn && !isMarked[i][j] {
				isSuicideMove = true
				break
			}
		}
	}

	if isSuicideMove {
		fmt.Println("Illegal: You cannot make a suicide move")
		return false
	}

	// TODO: We temporarily disable KO check

	/*isIdenticalZero := true
	isIdenticalOne := true	
	for i := 0; i < BoardInstance.BoardSize; i++ {
		for j := 0; j < BoardInstance.BoardSize; j++ {
			if tmpBoard[i][j] != BoardInstance.PreviousZero[i][j] {
				isIdenticalZero = false
			}
			if tmpBoard[i][j] != BoardInstance.PreviousOne[i][j] {
				isIdenticalOne = false
			}
		}
	}
	
	if isIdenticalZero || isIdenticalOne {
		fmt.Println("Illegal: KO is not allowed")
		return false
	}
	*/

	for i := 0; i < BoardInstance.BoardSize; i++ {
		for j := 0; j < BoardInstance.BoardSize; j++ {
			BoardInstance.BoardValue[i][j] = tmpBoard[i][j]
		}
	}
	
	// store the previous board
	if BoardInstance.ReassignPosition == 0 {
		BoardInstance.PreviousZero = BoardInstance.BoardValue
		BoardInstance.ReassignPosition = 1
	} else {
		BoardInstance.PreviousOne = BoardInstance.BoardValue
		BoardInstance.ReassignPosition = 0
	}
	return true
}

func main() {
	// Default size is 9
	b := Board{}
	b.BoardSize = 9

	b.BoardValue = make([][]int, b.BoardSize, b.BoardSize)
	b.PreviousZero = make([][]int, b.BoardSize, b.BoardSize)
	b.PreviousOne = make([][]int, b.BoardSize, b.BoardSize)

	for i := 0; i < b.BoardSize; i++ {
		b.BoardValue[i] = make([]int, b.BoardSize)
		b.PreviousZero[i] = make([]int, b.BoardSize)
		b.PreviousOne[i] = make([]int, b.BoardSize)
	}

	var turn = WHITE
	for ; true ;  {
		var x, y int
		_, err := fmt.Scanf("%d %d", &x, &y)
		if err != nil {
			// do nothing for now
		}		
		isValid := b.placeStone(x, y, turn)
		b.printBoard()

		if isValid {
			if turn == WHITE {
				turn = BLACK
			} else {
				turn = WHITE
			}		
		}	
	}
}